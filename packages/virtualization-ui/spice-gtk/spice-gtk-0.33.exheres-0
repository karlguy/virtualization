# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 1.11 1.10 ] ]

SUMMARY="A Gtk client and libraries for SPICE remote desktop servers."

HOMEPAGE="http://spice-space.org/page/Spice-Gtk"
DOWNLOADS="http://www.spice-space.org/download/gtk/${PNV}.tar.bz2"

LICENCES="LGPL-2.1"
SLOT="3.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    (
        gstreamer  [[ description = [ Use gstreamer for the audio backend ] ]]
        pulseaudio [[ description = [ Use pulseaudio for the audio backend ] ]]
    ) [[ number-selected = at-most-one ]]
    lz4 [[ description = [ Enable lz4 compression ] ]]
    polkit [[ description = [ Enable PolicyKit support (for the usb acl helper ] ]]
    sasl [[ description = [ use cyrus SASL for authentication ] ]]
    smartcard [[ description = [ Enable smartcard support ] ]]
    usbredir [[ description = [ Enable USB redirection support ] ]]
    vapi [[ description = [ Generate vala bindings ] requires = [ gobject-introspection ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/pyparsing
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext[>=0.17]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.4] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
        vapi? ( dev-lang/vala:*[>=0.14] )
    build+run:
        dev-db/sqlite:3
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2[>=2.36]
        dev-libs/icu:=
        dev-libs/json-c
        dev-libs/libffi
        dev-libs/libxml2:2.0
        media-libs/celt:0.5.1[>=0.5.1.1]
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libpng:=
        media-libs/libsndfile
        media-libs/opus
        net-libs/libasyncns
        sys-apps/dbus
        sys-apps/usbutils-data
        sys-libs/gdbm
        sys-libs/libcap
        virtualization-lib/spice-protocol[>=0.12.11]
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/cairo[>=1.2.0]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.12]
        x11-libs/harfbuzz
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXau
        x11-libs/libxcb
        x11-libs/libXdamage
        x11-libs/libXdmcp
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libXxf86vm
        x11-libs/pango
        x11-libs/pixman:1[>=0.17.7]
        x11-apps/xrandr
        !virtualization-ui/spice-gtk2:2.0 [[
            description = [ Install the same things ]
            resolution = uninstall-blocked-after
        ]]
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        lz4? ( app-arch/lz4 )
        polkit? (
            sys-apps/acl
            sys-auth/polkit:1[>=0.96]
        )
        pulseaudio? ( media-sound/pulseaudio )
        sasl? ( net-libs/cyrus-sasl )
        smartcard? ( dev-libs/libcacard[>=0.1.2] )
        usbredir? (
            dev-libs/usbredir[>=0.4.2]
            sys-apps/systemd
            virtual/usb:1
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

BUGS_TO="philantrop@exherbo.org"

src_prepare() {
    edo sed -i -e "s:\`pkg-config:\`${PKG_CONFIG}:g" src/Makefile.am src/Makefile.in
    edo sed -i -e "/CODE_GENERATOR_BASEDIR/s:pkg-config:${PKG_CONFIG}:" spice-common/configure.ac

    autotools_src_prepare
}

# python bindings don't get built with the gtk3 version
src_configure() {
    econf \
        --enable-controller \
        --enable-dbus \
        --disable-epoxy \
        --disable-gstvideo \
        --disable-webdav \
        --disable-werror \
        --with-gtk=3.0 \
        --with-coroutine=ucontext \
        --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}" \
        --with-udevdir="${UDEVRULESDIR/rules.d}" \
        --with-usb-ids-path=/usr/share/misc/usb.ids \
        --without-x11 \
        $(option_enable gobject-introspection introspection) \
        $(option_enable gstreamer gstaudio) \
        $(option_enable gtk-doc) \
        $(option_enable lz4) \
        $(option_enable polkit) \
        $(option_enable pulseaudio pulse) \
        $(option_enable smartcard) \
        $(option_enable usbredir) \
        $(option_enable vapi vala) \
        $(option_with sasl)
}

